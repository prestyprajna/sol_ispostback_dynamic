﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_IsPostBack_dynamic
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Page.IsPostBack==false)
            {
                Response.Write("Hello world");

                //bind labelname
                lblName.Text = "Hello Label";

                //bind spanName
                spanName.InnerText = "Hello Span"; 
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            lblName.EnableViewState = false;
            spanName.EnableViewState = true;
        }
    }
}